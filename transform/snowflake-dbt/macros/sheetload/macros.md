{% docs cleanup_certificates %}
This macro cleans up and standardizes data from Certifications created by the L&D team in the People Group.
It takes the certificate name as a string and the email address column from the raw data as two arguments.
{% enddocs %}
